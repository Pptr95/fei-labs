﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;

namespace PRLab.FEI
{

    [AlgorithmInfo("Negativo Grayscale", Category = "FEI")]
    public class NegativeImage : ImageOperation<Image<byte>, Image<byte>>
    {
        public NegativeImage(Image<byte> img)
        {
            InputImage = img;
        }

        public override void Run()
        {
            Result = new Image<byte>(InputImage.Width, InputImage.Height);
            for (int i = 0; i < InputImage.PixelCount; i++)
            {
                Result[i] = (255 - InputImage[i]).ClipToByte();
            }
        }
    }

    [AlgorithmInfo("Negativo RGB", Category = "FEI")]
    public class NegativeRgbImage : ImageOperation<RgbImage<byte>, RgbImage<byte>>
    {
        public override void Run()
        {
            Result = new RgbImage<byte>(InputImage.Width, InputImage.Height);
            for (int i = 0; i < InputImage.PixelCount; i++)
            {
                Result.RedChannel[i] = (255 - InputImage.RedChannel[i]).ClipToByte();
                Result.GreenChannel[i] = (255 - InputImage.GreenChannel[i]).ClipToByte();
                Result.BlueChannel[i] = (255 - InputImage.BlueChannel[i]).ClipToByte();
            }
        }
    }

    [AlgorithmInfo("Brightness Varation", Category = "FEI")]
    public class BrightnessVaration : ImageOperation<Image<byte>, Image<byte>>
    {
        private int var;

        [AlgorithmParameter]
        public int Var
        {
            get
            {
                return var;
            }
            set
            {
                if (value < -100 || value > 100)
                {
                    throw new Exception("Wrong range");
                }
                var = value;
            }
        }



        public override void Run()
        {
            PixelMapping<byte, byte> pm = p => (p + Var * 255 / 100).ClipToByte();
            Result = new LookupTableTransform<byte>(InputImage, pm).Execute();
        }
    }

    [AlgorithmInfo("From greyscale to rgb", Category = "FEI")]
    public class FromGrayToRgb : ImageOperation<Image<byte>, RgbImage<byte>>
    {

        [AlgorithmOutput]
        public RgbImage<byte> Result1 { get; private set; }

        [AlgorithmOutput]
        public Image<byte> Result2 { get; private set; }

        public override void Run()
        {
            ///from greyscale to rbg, following LookupTables.Spectrum mapping
            Result1 = new RgbLookupTableTransform<byte>(InputImage, LookupTables.Spectrum).Execute();

            /// from rgb to greyscale
            Result2 = Result1.RedChannel;
            Result2 = Result1.GreenChannel;
            Result2 = Result1.BlueChannel;
        }
    }

    [AlgorithmInfo("MY binarization", Category = "FEI")]
    public class Binarization : ImageOperation<Image<byte>, Image<byte>>
    {
        private int thr;

        [AlgorithmParameter]
        public int Thr
        {
            get
            {
                return thr;
            }
            set
            {
                if (value < 0 || value > 255)
                {
                    throw new Exception("Wrong parameter");
                }
                thr = value;
            }
        }

        public override void Run()
        {
            PixelMapping<byte, byte> pm = p => (p <= Thr ? 0 : 255).ClipToByte();
            Result = new LookupTableTransform<byte>(InputImage, pm).Execute();
        }
    }

    [AlgorithmInfo("MY ABS + AND op. ", Category = "FEI")]
    public class ABSoperation : ImageOperation<Image<byte>, Image<byte>, Image<byte>>
    {
        [AlgorithmOutput]
        public Image<byte> Result1 { get; private set; }


        public override void Run()
        {
            Result1 = new Image<byte>(InputImage1.Width, InputImage1.Height);
            for (int i = 0; i<InputImage1.PixelCount; i++)
            {
                Result1[i] = Math.Abs(InputImage1[i] - InputImage2[i]).ClipToByte(); 
            }

            Result = new Image<byte>(InputImage1.Width, InputImage1.Height);
            for (int i = 0; i < InputImage1.PixelCount; i++)
            {
                Result[i] = (InputImage1[i] & InputImage2[i]).ClipToByte();
            }
        }

    }

    [AlgorithmInfo("MY base histogram", Category = "FEI")]
    public class MyHistogram : ImageOperation<Image<byte>, Histogram>
    { 
        public override void Run()
        {
            Result = new Histogram();
            foreach(byte b in InputImage)
            {
                Result[b]++; 
            }
        }
    }

    [AlgorithmInfo("MY Contrast stretching", Category = "FEI")]
    public class ContrastStretching : ImageOperation<Image<byte>, Image<byte>>
    { // to improve
        [AlgorithmOutput]
        public Histogram Result2 { get; private set; }

        public override void Run()
        {
            int min = InputImage[0];
            int max = InputImage[0];
            for(int i=0; i<InputImage.PixelCount; i++)
            {
                if(InputImage[i] < min)
                {
                    min = InputImage[i];
                }
                if (InputImage[i] > max)
                {
                    max = InputImage[i];
                }
            }
            int diff = 160; // instead of diff = max - min, i put an hardcoded number off diff in a way to cutt 255 and 0 values and checkout the difference
            if(diff > 0)
            {
                Result = new LookupTableTransform<byte>(InputImage, p => (255 * (p - min) / diff).ClipToByte()).Execute();
                Result2 = new Histogram();
                foreach (byte b in Result)
                {
                    Result2[b]++;
                }
            } else
            {
                Result = InputImage.Clone();
            }
        }
    }

}
