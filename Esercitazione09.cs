﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;
using System.ComponentModel;
using BioLab.GUI.UserControls;

namespace PRLab.FEI
{

    [AlgorithmInfo("Morfologia: Dilatazione", Category = "FEI")]
    public class Dilatazione : MorphologyOperation
    {
        public Dilatazione(Image<byte> inputImage, Image<byte> structuringElement, byte foreground)
          : base(inputImage, structuringElement, foreground)
        {  
        }

        public Dilatazione(Image<byte> inputImage, Image<byte> structuringElement)
          : base(inputImage, structuringElement)
        {  
        }

        public Dilatazione()
        {
        }
        public override void Run()
        {
            Result = new Image<byte>(InputImage.Width, InputImage.Height, (byte)(255 - Foreground));
            // Costruisce l'array degli offset dell'elemento strutturante riflesso
            int[] elementOffsets = MorphologyStructuringElement.CreateOffsets(
             StructuringElement, InputImage, true);
            // Crea un cursore per scorrere l'immagine escludendo i pixel di bordo
            var pixelCursor = new ImageCursor(
            StructuringElement.Width / 2,
            StructuringElement.Height / 2,
            InputImage.Width - 1 - StructuringElement.Width / 2,
            InputImage.Height - 1 - StructuringElement.Height / 2,
            InputImage);
            do
            {
                foreach (int offset in elementOffsets)
                {
                    if (InputImage[pixelCursor + offset] == Foreground)
                    {
                        Result[pixelCursor] = Foreground;
                        break; // esce dal foreach
                    }
                }
            } while (pixelCursor.MoveNext());
        }
    }

    [AlgorithmInfo("Morfologia: Erosione", Category = "FEI")]
    public class Erosione : MorphologyOperation
    {
        public Erosione(Image<byte> inputImage, Image<byte> structuringElement, byte foreground)
          : base(inputImage, structuringElement, foreground)
        {
        }

        public Erosione(Image<byte> inputImage, Image<byte> structuringElement)
          : base(inputImage, structuringElement)
        {
        }

        public Erosione()
        {
        }

        public override void Run()
        {
            byte background = (byte)(255 - Foreground);
            Result = new Image<byte>(InputImage.Width, InputImage.Height, background);
            // Costruisce l'array degli offset dell'elemento strutturante non riflesso
            int[] elementOffsets = MorphologyStructuringElement.CreateOffsets(
             StructuringElement, InputImage, false);
            // Crea un cursore per scorrere l'immagine escludendo i pixel di bordo
            var pixelCursor = new ImageCursor(
            StructuringElement.Width / 2,
            StructuringElement.Height / 2,
            InputImage.Width - 1 - StructuringElement.Width / 2,
            InputImage.Height - 1 - StructuringElement.Height / 2,
            InputImage);
            do
            {
                int flag = 0;
                foreach (int offset in elementOffsets)
                {
                    if (InputImage[pixelCursor + offset] == background)
                    {
                        flag++;
                    }
                }
                if(flag == 0)
                {
                    Result[pixelCursor] = Foreground;
                }
            } while (pixelCursor.MoveNext());
        }
    }

    [AlgorithmInfo("Morfologia: Apertura", Category = "FEI")]
    public class Apertura : MorphologyOperation
    {
        public Apertura(Image<byte> inputImage, Image<byte> structuringElement, byte foreground)
          : base(inputImage, structuringElement, foreground)
        {
        }

        public Apertura(Image<byte> inputImage, Image<byte> structuringElement)
          : base(inputImage, structuringElement)
        {
        }

        public Apertura()
        {
        }

        public override void Run()
        {
            Image<byte> erosioned = new Erosione(InputImage, StructuringElement, Foreground).Execute();
            Result = new Dilatazione(erosioned, StructuringElement, Foreground).Execute();
        }
    }

    [AlgorithmInfo("Morfologia: Chiusura", Category = "FEI")]
    public class Chiusura : MorphologyOperation
    {
        public Chiusura(Image<byte> inputImage, Image<byte> structuringElement, byte foreground)
          : base(inputImage, structuringElement, foreground)
        {
        }

        public Chiusura(Image<byte> inputImage, Image<byte> structuringElement)
          : base(inputImage, structuringElement)
        {
        }

        public Chiusura()
        {
        }

        public override void Run()
        {
            Image<byte> dilationed = new Dilatazione(InputImage, StructuringElement, Foreground).Execute();
            Result = new Erosione(dilationed, StructuringElement, Foreground).Execute();
        }
    }

    [AlgorithmInfo("Morfologia: Estrazione bordo", Category = "FEI")]
    public class EstrazioneBordo : MorphologyOperation
    {
        public EstrazioneBordo(Image<byte> inputImage, Image<byte> structuringElement, byte foreground)
          : base(inputImage, structuringElement, foreground)
        {
        }

        public EstrazioneBordo(Image<byte> inputImage, Image<byte> structuringElement)
          : base(inputImage, structuringElement)
        {
        }

        public EstrazioneBordo()
        {
        }

        public override void Run()
        {
            Image<byte> erosioned = new Erosione(InputImage, StructuringElement, Foreground).Execute();
            /*Result = new ImageArithmetic(InputImage, erosioned, ImageArithmeticOperation.Difference).Execute();*/
            Result = new Image<byte>(erosioned.Width, erosioned.Height);
            for(int i = 0; i < erosioned.PixelCount; i++)
            {
                Result[i] = (InputImage[i] - erosioned[i]).ClipToByte();
            }

        }
    }

    [AlgorithmInfo("Morfologia: Hit or miss", Category = "FEI")]
    public class HitOrMiss : MorphologyOperation
    {
        [AlgorithmParameter]
        public Image<byte> StructuringElement2 { get; set; }

        public HitOrMiss(Image<byte> inputImage, Image<byte> structuringElement, byte foreground)
          : base(inputImage, structuringElement, foreground)
        {
        }

        public HitOrMiss(Image<byte> inputImage, Image<byte> structuringElement)
          : base(inputImage, structuringElement)
        {
        }

        public HitOrMiss()
        {
        }

        public override void Run()
        {//don't know how to set properly structural element for testing
            Image<byte> fs1 = new Erosione(InputImage, StructuringElement, Foreground).Execute();
            Image<byte> fs2 = new Erosione(new NegativeImage(InputImage).Execute(), StructuringElement2, Foreground).Execute();
            Result = new ImageArithmetic(fs1, fs2, ImageArithmeticOperation.And).Execute();
        }
    }
}
