﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;
using System.ComponentModel;
using BioLab.GUI.UserControls;

namespace PRLab.FEI
{ 
    [AlgorithmInfo("Etichettatura delle componenti connesse", Category = "FEI")]
    public class EtichettaturaComponentiConnesse : TopologyOperation<ConnectedComponentImage>
    {
        public EtichettaturaComponentiConnesse(Image<byte> inputImage, byte foreground, MetricType metric)
          : base(inputImage, foreground)
        {
            Metric = metric;
        }

        public EtichettaturaComponentiConnesse(Image<byte> inputImage)
          : this(inputImage, 255, MetricType.Chessboard)
        {
        }

        public EtichettaturaComponentiConnesse()
        {
            Metric = MetricType.Chessboard;
        }

        public override void Run()
        {
            Result = new ConnectedComponentImage(InputImage.Width, InputImage.Height, -1);
            var cursor = new ImageCursor(InputImage, 1); // per semplicità ignora i bordi (1 pixel)            
            int[] neighborLabels = new int[Metric == MetricType.CityBlock ? 2 : 4];
            int nextLabel = 0;
            var equivalences = new DisjointSets(InputImage.PixelCount);
            do
            { // prima scansione
                if (InputImage[cursor] == Foreground)
                {
                    int labelCount = 0;
                    if (Result[cursor.West] >= 0) neighborLabels[labelCount++] = Result[cursor.West];
                    if (Result[cursor.North] >= 0) neighborLabels[labelCount++] = Result[cursor.North];
                    if (Metric == MetricType.Chessboard)
                    {   // anche le diagonali
                        if (Result[cursor.Northwest] >= 0) neighborLabels[labelCount++] = Result[cursor.Northwest];
                        if (Result[cursor.Northeast] >= 0) neighborLabels[labelCount++] = Result[cursor.Northeast];
                    }
                    if (labelCount == 0)
                    {
                        equivalences.MakeSet(nextLabel); // crea un nuovo set
                        Result[cursor] = nextLabel++; // le etichette iniziano da 0
                    }
                    else
                    {
                        int l = Result[cursor] = neighborLabels[0]; // seleziona la prima
                        for (int i = 1; i < labelCount; i++) // equivalenze
                            if (neighborLabels[i] != l)
                                equivalences.MakeUnion(neighborLabels[i], l); // le rende equivalenti
                    }
                }
            } while (cursor.MoveNext());

            //seconda scasione
            int totalLabels;
            int[] corresp = equivalences.Renumber(nextLabel, out totalLabels);
            // seconda e ultima scansione
            cursor.Restart();
            do
            {
                int l = Result[cursor];
                if (l >= 0)
                {
                    Result[cursor] = corresp[l];
                }
            } while (cursor.MoveNext());
            Result.ComponentCount = totalLabels;

        }

        [AlgorithmParameter]
        [DefaultValue(MetricType.Chessboard)]
        public MetricType Metric { get; set; }
    }

    
    [AlgorithmInfo("Informazioni componenti connesse", Category = "FEI")]
    public class InformazioniComponentiConnesse : Algorithm
    {
        [AlgorithmInput]
        public Image<byte> InputImage { get; set; }

        [AlgorithmInput]
        public byte Foreground { get; set; }

        public int Background = -1;

        [AlgorithmOutput]
        public int NumConnectedComponent { get; set; }

        [AlgorithmOutput]
        public int MinArea { get; set; }

        [AlgorithmOutput]
        public double AvgArea { get; set; }

        [AlgorithmOutput]
        public int MaxArea { get; set; }

        [AlgorithmOutput]
        public double AvgPerimeter { get; set; }

        public override void Run()
        {
            ConnectedComponentImage ec = new EtichettaturaComponentiConnesse(InputImage).Execute();
            NumConnectedComponent = ec.ComponentCount;

            int[] areaForEachComponent = new int[NumConnectedComponent];
            foreach (int i in ec)
            {
                if (i != Background)
                {
                    areaForEachComponent[i]++;
                }
            }
            MinArea = areaForEachComponent.Min();
            MaxArea = areaForEachComponent.Max();
            AvgArea = areaForEachComponent.Average();

            int[] perimeterForEachComponent = new int[NumConnectedComponent];
            ImageCursor cursor = new ImageCursor(ec, 1);
            do
            {
                if (ec[cursor] != Background)
                {
                    if (IsPixelBorder(cursor, ec))
                    {
                        perimeterForEachComponent[ec[cursor]]++;
                        break;
                    }
                }
            } while (cursor.MoveNext());
            AvgPerimeter = perimeterForEachComponent.Average();
        }

        private bool IsPixelBorder(ImageCursor cursor, ConnectedComponentImage ec)
        {
            for (ChessboardDirection d = ChessboardDirection.East; d <= ChessboardDirection.Southeast; d++)
            {
                if (ec[cursor.GetAt(d)] == Background)
                {
                    return true;
                }
            }
            return false;
            /*return ec[cursor.North] == Background || ec[cursor.East] == Background || ec[cursor.South] == Background || ec[cursor.West] == Background
            || ec[cursor.Northeast] == Background || ec[cursor.Northwest] == Background || ec[cursor.Southeast] == Background || ec[cursor.Southwest] == Background;
            */
        }
    }

    [AlgorithmInfo("Esame 22/02/2011 es5 ", Category = "FEI")]
    public class InformazioniComponentiConnesseEsame : Algorithm
    {
        [AlgorithmInput]
        public Image<byte> InputImage { get; set; }

        [AlgorithmParameter]
        public byte Foreground { get; set; }

        [AlgorithmOutput]
        public int CompArea { get; set; }

        [AlgorithmOutput]
        public int CompPer { get; set; }

        public override void Run()
        {
            ConnectedComponentImage cci = new ConnectedComponentsLabeling(InputImage, Foreground, MetricType.Chessboard).Execute();
            int[] areas = new int[cci.ComponentCount];
            foreach(var b in cci)
            {
                if(b > -1)
                {
                    areas[b]++;
                }
            }
            foreach (var b in areas)
            {
                if (b > 500) // changed numero for testing
                {
                    CompArea++;
                }
            }

            int[] perimeters = new int[cci.ComponentCount];
            ImageCursor cursor = new ImageCursor(cci, 1);
            do
            {
                if (cci[cursor] > -1)
                {
                    var direction = ChessboardDirection.East;
                    for (; direction <= ChessboardDirection.Southeast; direction++)
                    {
                        if (cci[cursor.GetAt(direction)] == -1)
                        {
                            perimeters[cci[cursor]]++;
                            break;
                        }
                    }
                }
            } while (cursor.MoveNext());
            foreach(var b in perimeters)
            {
                if(b > 200) // changed number for testing
                {
                    CompPer++;
                }
            }

        }
    }

    [AlgorithmInfo("Esame 21/06/2011 es5 ", Category = "FEI")]
    public class InformazioniComponentiConnesseEsame2 : Algorithm
    {
        [AlgorithmInput]
        public Image<byte> InputImage { get; set; }

        [AlgorithmOutput]
        public Image<byte> Result { get; set; }


        [AlgorithmParameter]
        public byte Foreground { get; set; }

        public override void Run()
        {
            byte background = (byte)(255 - Foreground);
            ConnectedComponentImage cci = new ConnectedComponentsLabeling(InputImage, background, MetricType.Chessboard).Execute();
            int[] areas = new int[cci.ComponentCount];
            foreach (var b in cci)
            {
                if (b > -1)
                {
                    areas[b]++;
                }
            }
            int maxArea = areas.Max();
            if (areas.Length <= 1)
            {
                return;
            }
            Result = InputImage.Clone();
         /*  solution using cursor
          *  ImageCursor cursor = new ImageCursor(cci);
            do
            {
                if(cci[cursor] != -1 && areas[cci[cursor]] < maxArea)
                {
                    Result[cursor] = Foreground;
                }
            } while (cursor.MoveNext());*/
            for (int i = 0; i < InputImage.PixelCount; i++)
            {
                if (cci[i] != -1 && areas[cci[i]] < maxArea)
                {
                    Result[i] = Foreground;
                }
            }
        }
    }

    [AlgorithmInfo("Esame 27/09/2011 es4 ", Category = "FEI")]
    public class InformazioniComponentiConnesseEsame3 : ImageOperation<Image<byte>, Image<byte>>
    {
        public InformazioniComponentiConnesseEsame3(Image<byte> inputImage)
        {
            InputImage = inputImage;
        }
        public override void Run()
        {
            Result = InputImage.Clone();
            ImageCursor borderDiscard = new ImageCursor(InputImage, 5 / 2);
            int i = 0;
            int j = 0;
            int sum;
            do
            {
                ImageCursor cursor = new ImageCursor(InputImage, i, j, 5, 5);
                sum = 0;
                do
                {
                    sum = sum + InputImage[cursor];
                } while (cursor.MoveNext());
                /*
                 *Binarization part
                 */
                if(InputImage[borderDiscard] > (sum/(5 * 5)))
                {
                    Result[borderDiscard] = 255;
                } else
                {
                    Result[borderDiscard] = 0;
                }
                /*
                 * Test assigned directly the value of the average to test if is bluring
                 * 
                 * Result[borderDiscard] = (sum / (5 * 5)).ClipToByte();
                 */
                if (i < InputImage.Width - 5)
                {
                    i++;
                } else
                {
                    i = 0;
                    j++;
                }
            } while (borderDiscard.MoveNext());
        }

    }

    

    [AlgorithmInfo("Esame 27/01/2012 es4 ", Category = "FEI")]
    public class InformazioniComponentiConnesseEsame4 : ImageOperation<Image<byte>, Image<byte>>
    {
        public double AvgPixel { get; set; }

        public InformazioniComponentiConnesseEsame4(Image<byte> inputImage)
        {
            InputImage = inputImage;
        }

        public override void Run()
        {
            Result = InputImage.Clone();
            int sum = 0;
            foreach (byte b in InputImage)
            {
                sum += b;
            }
            AvgPixel = sum / InputImage.PixelCount;
            ImageCursor discard = new ImageCursor(InputImage, 5 / 2);
            int i = 0;
            int j = 0;
            do
            {
                ImageCursor cursor = new ImageCursor(InputImage, i, j, 5, 5);
                sum = 0;
                do
                {
                    sum = sum + InputImage[cursor];
                } while (cursor.MoveNext());
                int avg = sum / (5 * 5);
                int min = Math.Min(avg, (int)AvgPixel);

                
                //Binarization part
                 
                if (InputImage[discard] > min)
                {
                    Result[discard] = 255;
                }
                else
                {
                    Result[discard] = 0;
                }
                
                  //Test assigned directly the value of the average to test if is bluring
                  /*
                  Result[discard] = (avg).ClipToByte();
                  */
                 
                if (i < InputImage.Width - 5)
                {
                    i++;
                }
                else
                {
                    i = 0;
                    j++;
                }
            } while (discard.MoveNext());
        }

    }

    [AlgorithmInfo("Esame 17/07/2012 es4 ", Category = "FEI")]
    public class InformazioniComponentiConnesseEsame5 : TopologyOperation<Image<double>>
    {  
        public override void Run()
        {
            ConnectedComponentImage cci = new ConnectedComponentsLabeling(InputImage, Foreground, MetricType.Chessboard).Execute();
            int[] areas = new int[cci.ComponentCount];
            foreach (var b in cci)
            {
                if (b > -1)
                {
                    areas[b]++;
                }
            }
            int[] perimeters = new int[cci.ComponentCount];
            ImageCursor cursor = new ImageCursor(cci, 1);
            do
            {
                if (cci[cursor] > -1)
                {
                    var direction = ChessboardDirection.East;
                    for (; direction <= ChessboardDirection.Southeast; direction++)
                    {
                        if (cci[cursor.GetAt(direction)] == -1)
                        {
                            perimeters[cci[cursor]]++;
                            break;
                        }
                    }
                }
            } while (cursor.MoveNext());
            cursor.Restart();
            Result = new Image<double>(InputImage.Width, InputImage.Height);
            do
            {
                if (cci[cursor] > -1)
                {
                    Result[cursor] = areas[cci[cursor]] / perimeters[cci[cursor]];
                }
                else
                {
                    Result[cursor] = 0;
                }
            } while (cursor.MoveNext());

        }
    }


    [AlgorithmInfo("Esame 17/07/2012 es5 ", Category = "FEI")]
    public class InformazioniComponentiConnesseEsame6 : TopologyOperation<Image<byte>>
    {
        public override void Run()
        {
            ConnectedComponentImage cci = new ConnectedComponentsLabeling(InputImage, Foreground, MetricType.Chessboard).Execute();
            int[] areas = new int[cci.ComponentCount];
            foreach (var b in cci)
            {
                if (b > -1)
                {
                    areas[b]++;
                }
            }
            int[] perimeters = new int[cci.ComponentCount];
            ImageCursor cursor = new ImageCursor(cci, 1);
            do
            {
                if (cci[cursor] > -1)
                {
                    var direction = ChessboardDirection.East;
                    for (; direction <= ChessboardDirection.Southeast; direction++)
                    {
                        if (cci[cursor.GetAt(direction)] == -1)
                        {
                            perimeters[cci[cursor]]++;
                            break;
                        }
                    }
                }
            } while (cursor.MoveNext());
            double[] ap = new double[cci.ComponentCount];
            for(int i = 0; i< areas.Length; i++)
            {
                ap[i] = areas[i] / perimeters[i];
            }
            double totAvgAP = ap.Average();
            cursor.Restart();
            Result = new Image<byte>(InputImage.Width, InputImage.Height);
            do
            {
                if (cci[cursor] > -1)
                {
                    if(ap[cci[cursor]] > totAvgAP)
                    {
                        Result[cursor] = 255;
                    } else
                    {
                        Result[cursor] = 0;
                    }
                }
                else
                {
                    Result[cursor] = 0;
                }
            } while (cursor.MoveNext());

        }
    }

    [AlgorithmInfo("Elimina piccole componentiX", Category = "FEI")]
    public class EliminaPiccoleComponenti : TopologyOperation<Image<byte>>
    {
        [AlgorithmParameter]
        public int AreaMinima { get; set; }

        public EliminaPiccoleComponenti(int areaMinima)
        {
            AreaMinima = areaMinima;
        }

        public override void Run()
        {
            ConnectedComponentImage ec = new EtichettaturaComponentiConnesse(InputImage, Foreground, MetricType.Chessboard).Execute();
            byte background = (255 - Foreground).ClipToByte();
            Result = new ImageBorderFilling<byte>(InputImage, 1, background, false).Execute();
            int[] area = new int[ec.ComponentCount];
            foreach (int p in ec)
            {
                if (p != -1)
                {
                    area[p]++;
                }
            }
            for (int i = 0; i < Result.PixelCount; i++)
            {
                if (ec[i] > -1 && area[ec[i]] < AreaMinima)
                {
                    Result[i] = background;
                }
            }
          /* previous and bad solution
           * for (int i = 0; i < area.Length; i++)
            {
                if (area[i] < AreaMinima)
                {
                    for (int j = 0; j < ec.PixelCount; j++)
                    {
                        if(ec[j] == i)
                        {
                            Result[j] = 0;
                        }
                    }
                }
            }*/
        }
    }
}
