﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;
using System.ComponentModel;

namespace PRLab.FEI
{
    /*
      [AlgorithmInfo("Calcolo Istogramma", Category = "FEI")]
      public class HistogramBuilder : ImageOperation<Image<byte>, Histogram>
      {
          [AlgorithmParameter]
          [DefaultValue(false)]
          public bool Sqrt { get; set; }

          [AlgorithmParameter]
          [DefaultValue(0)]
          public int SmoothWindowSize { get; set; }

          public HistogramBuilder(Image<byte> inputImage)
          {
              InputImage = inputImage;
          }

          public HistogramBuilder(Image<byte> inputImage, bool sqrt, int smoothWindowSize)
          {
              InputImage = inputImage;
              Sqrt = sqrt;
              SmoothWindowSize = smoothWindowSize;
          }

          public override void Run()
          {
              Result = new Histogram();
              foreach (byte b in InputImage)
              {
                  Result[b]++;
              }

              if (Sqrt)
              {
                  for (int i = 0; i < Result.Count(); i++)
                  {
                      Result[i] = (int)(Math.Round(Math.Sqrt(Result[i])));
                  }
              }
              if (SmoothWindowSize > 0)
              {

                  Histogram cloned = Result.Clone();
                  FirstPart(ref cloned);
                  LastPart(ref cloned);
                  CenterPart(ref cloned);
                  Result = cloned;
              }
          }

          private void FirstPart(ref Histogram hist)
          {
              for (int i = 0; i < SmoothWindowSize - 1; i++)
              {
                  int sum = 0;
                  int div = 0;
                  for (int j = i; j < SmoothWindowSize; j++, div++)
                  {
                      sum = sum + Result[j];
                  }
                  hist[i] = sum / div;
              }
          }

          private void LastPart(ref Histogram hist)
          {
              for (int i = hist.Count() - SmoothWindowSize; i < hist.Count() - 1; i++)
              {
                  int sum = 0;
                  int div = 0;
                  for (int j = i; j < hist.Count(); j++, div++)
                  {
                      sum = sum + Result[j];
                  }
                  hist[i] = sum / div;
              }
          }

        private void CenterPart(ref Histogram hist)
        {
            for (int i = SmoothWindowSize; i < hist.Count() - SmoothWindowSize; i++)
            {
                int sum = 0;
                int j;
                for (j = i - SmoothWindowSize; j <= i; j++)
                {
                    sum = sum + Result[j];
                }
                for (; j < i + SmoothWindowSize + 1; j++)
                {
                    sum = sum + Result[j];
                }
                hist[i] = sum / (2 * SmoothWindowSize + 1);
            }

        }
      }
    */
    [AlgorithmInfo("Calcolo Istogramma", Category = "FEI")]
    public class HistogramBuilder : ImageOperation<Image<byte>, Histogram>
    {
        const int exceededNum = 256;

        [AlgorithmParameter]
        [DefaultValue(false)]
        public bool Sqrt { get; set; }

        [AlgorithmParameter]
        [DefaultValue(0)]
        public int SmoothWindowSize { get; set; }

        public HistogramBuilder(Image<byte> inputImage)
        {
            InputImage = inputImage;
        }

        public HistogramBuilder(Image<byte> inputImage, bool sqrt, int smoothWindowSize)
        {
            InputImage = inputImage;
            Sqrt = sqrt;
            SmoothWindowSize = smoothWindowSize;
        }
        public override void Run()
        {
            Histogram hist = new Histogram();
            for(int i = 0; i < InputImage.PixelCount; i++)
            {
                hist[InputImage[i]]++;
            }
            Result = hist;
            if(Sqrt)
            {
                for(int b = 0; b < Result.Count();b++)
                {
                    Result[b] = (Math.Sqrt(Result[b])).RoundAndClipToByte();
                }
            }
            
            if(SmoothWindowSize > 0)
            {
                int i = 0;
                int window = 2 * SmoothWindowSize + 1;
                int sum = 0;
                Histogram nhist = Result.Clone();
                int start = i - SmoothWindowSize;
                int end = i + SmoothWindowSize;
                int nwindow = window;
                for(; i < 255; i++)
                {
                    int copyStart = start;
                    sum = 0;
                    for(; copyStart < end; copyStart++)
                    {
                        if(copyStart < 0)
                        {
                            copyStart = 0;
                            nwindow = end + 1;
                        }
                        if(end > 255)
                        {
                            nwindow = exceededNum - start;
                        }
                        sum = sum + Result[copyStart];
                    }
                    nhist[i] = sum / nwindow;
                    if(nwindow != window)
                    {
                        nwindow = window;
                    }
                    end++;
                    start++;
                }
                Result = nhist;
            }
        }
    }

   [AlgorithmInfo("Binarization", Category = "FEI")]
        public class MyBinarization : ImageOperation<Image<byte>, Image<byte>>
        {
            private int thr = 128;
            [AlgorithmParameter]
            public int Thr
            {
                get
                {
                    return thr;
                }
                set
                {
                    if (value < 0 || value > 255)
                    {
                        throw new Exception("Insert a value between 0 and 255");
                    }
                    thr = value;
                }
            }

            public override void Run()
            {
                Result = new LookupTableTransform<byte>(InputImage, p => (p <= Thr ? 0 : 255).ClipToByte()).Execute();
            }
        }

        [AlgorithmInfo("Split rgb channel to grayscale", Category = "FEI")]
        public class SplitRgbToGrayScale : Algorithm
        {
            [AlgorithmInput]
            public RgbImage<byte> InputImage { get; set; }

            [AlgorithmOutput]
            public Image<byte> RedGrayScale { get; set; }

            [AlgorithmOutput]
            public Image<byte> GreenGrayScale { get; set; }

            [AlgorithmOutput]
            public Image<byte> BlueGrayScale { get; set; }



            public override void Run()
            {
                RedGrayScale = InputImage.RedChannel;
                GreenGrayScale = InputImage.GreenChannel;
                BlueGrayScale = InputImage.BlueChannel;
            }
        }

        [AlgorithmInfo("From three grasyscale to one rgb", Category = "FEI")]
        public class FromThreeGrayScaleToOneRgb : Algorithm
        {
            [AlgorithmInput]
            public Image<byte> Red { get; set; }

            [AlgorithmInput]
            public Image<byte> Green { get; set; }

            [AlgorithmInput]
            public Image<byte> Blue { get; set; }

            [AlgorithmOutput]
            public RgbImage<byte> Result { get; set; }

            public override void Run()
            {
                Result = new RgbImage<byte>(Red, Green, Blue);
            }
        }

        [AlgorithmInfo("Change rgb brightness", Category = "FEI")]
        public class BrightnessRgb : Algorithm
        {
            [AlgorithmInput]
            public RgbImage<byte> InputImage { get; set; }

            [AlgorithmOutput]
            public RgbImage<byte> Result { get; set; }

            private int var;
            [AlgorithmParameter]
            public int Var
            {
                get
                {
                    return var;
                }
                set
                {
                    if (value < 0 || value > 100)
                    {
                        throw new Exception("Insert a value between 0 and 100");
                    }
                    var = value;
                }
            }


            public override void Run()
            {
                /*
                 * Solution made without using BrightnessVaration
                 * Image<byte> red = new LookupTableTransform<byte>(InputImage.RedChannel, p => (p + (Var*255) / 100).ClipToByte()).Execute();
                  Image<byte> green = new LookupTableTransform<byte>(InputImage.GreenChannel, p => (p + (Var * 255) / 100).ClipToByte()).Execute();
                  Image<byte> blue = new LookupTableTransform<byte>(InputImage.BlueChannel, p => (p + (Var * 255) / 100).ClipToByte()).Execute();
                  Result = new RgbImage<byte>(red, green, blue);
                 */

                //Solution made using BrightnessVaration
                BrightnessVaration br = new BrightnessVaration { Var = Var };
                //br.Var = Var;
                br.InputImage = InputImage.RedChannel;
                Image<byte> r = br.Execute();
                br.InputImage = InputImage.GreenChannel;
                Image<byte> g = br.Execute();
                br.InputImage = InputImage.BlueChannel;
                Image<byte> b = br.Execute();
                Result = new RgbImage<byte>(r, g, b);
            }
        }

        [AlgorithmInfo("From Rgb to HSL", Category = "FEI")]
        public class FromRgbToHsl : Algorithm
        {
            [AlgorithmInput]
            public RgbImage<byte> InputImage { get; set; }

            [AlgorithmOutput]
            public Image<byte> Hue { get; set; }

            [AlgorithmOutput]
            public Image<byte> Saturation { get; set; }

            [AlgorithmOutput]
            public Image<byte> Lightness { get; set; }

            public override void Run()
            {
                HslImage<byte> h = InputImage.ToByteHslImage();
                Hue = h.HueChannel;
                Saturation = h.SaturationChannel;
                Lightness = h.LightnessChannel;
            }
        }

        [AlgorithmInfo("Optional 1", Category = "FEI")]
        public class Optional1 : Algorithm
        {
            [AlgorithmInput]
            public Image<byte> Hue { get; set; }

            [AlgorithmInput]
            public Image<byte> Saturation { get; set; }

            [AlgorithmInput]
            public Image<byte> Lightness { get; set; }

            [AlgorithmOutput]
            public RgbImage<byte> Result { get; set; }

            public override void Run()
            {
                Result = new HslImage<byte>(Hue, Saturation, Lightness).ToByteRgbImage();
            }
        }
    [AlgorithmInfo("Optional 2", Category = "FEI")]
    public class Optional2 : ImageOperation<Image<byte>, RgbImage<byte>>
    {
        [DefaultValue(128)]
        private byte hue;
        [AlgorithmParameter]
        public byte Hue
        {
            get
            {
                return hue;
            }
            set
            {
                if (value < 0 || value > 255)
                {
                    throw new Exception("Enter a value between 0 and 255 inclusive.");
                }
                hue = value;
            }
        }

        [DefaultValue(128)]
        private byte sat;
        [AlgorithmParameter]
        public byte Sat
        {
            get
            {
                return sat;
            }
            set
            {
                if (value < 0 || value > 255)
                {
                    throw new Exception("Enter a value between 0 and 255 inclusive.");
                }
                sat = value;
            }
        }


        public override void Run()
        {
            Result = new HslImage<byte>(new Image<byte>(InputImage.Width, InputImage.Height, Hue), new Image<byte>(InputImage.Width, InputImage.Height, Sat), InputImage).ToByteRgbImage();

        }

    }
}
