﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;
using System.ComponentModel;
using BioLab.GUI.UserControls;

namespace PRLab.FEI
{

    [AlgorithmInfo("Convoluzione (da immagine byte a immagine intX)", Category = "FEI")]
    [CustomAlgorithmPreviewParameterControl(typeof(SimpleConvolutionParameterControl))]
    public class ConvoluzioneByteInt : Convolution<byte, int, ConvolutionFilter<int>>
    {
        public ConvoluzioneByteInt(Image<byte> inputImage, ConvolutionFilter<int> filter)
         : base(inputImage, filter, 0)
        {
        }
        public ConvoluzioneByteInt(Image<byte> inputImage, ConvolutionFilter<int> filter, int additionalBorder)
         : base(inputImage, filter, additionalBorder)
        {
        }
        public override void Run()
        {
            Result = new Image<int>(InputImage.Width, InputImage.Height);
            for (int i = 0; i < InputImage.Height; i++)
            {
                for (int j = 0; j < InputImage.Width; j++)
                {
                    for (int h = -51 / 2; h < 51 / 2; h++)
                    {
                        for (int k = -51 / 2; k < 51 / 2; k++)
                        {
                            if (i + h > 0 && k + j > 0 && (i + h) * InputImage.Width + k + j < InputImage.PixelCount)
                            {
                                Result[i, j] += InputImage[i + h, j + k];
                            }
                        }
                    }
                    Result[i, j] = (Result[i, j] / (51 * 51)).ClipToByte();
                }
            }
        }
        /* public override void Run()
         {
             Result = new Image<int>(InputImage.Width, InputImage.Height);
             int w = InputImage.Width;
             int h = InputImage.Height;
             int m = Filter.Size;
             int m2 = m / 2;
             int i1 = m2;
             int i2 = h - m2 - 1;
             int j1 = m2;
             int j2 = w - m2 - 1;



             int filterSize = Filter.Size * Filter.Size;
             int[] FOff = new int[filterSize];
             int[] FVal = new int[filterSize];
             int currentArrayPos = 0;

             for(int y = 0; y < Filter.Size; y++)
             {
                 for (int x = 0; x < Filter.Size; x++)
                 {
                     if (Filter[y, x] != 0)
                     {
                         FOff[currentArrayPos] = (m2 - y) * w + (m2 - x);
                         FVal[currentArrayPos] = Filter[y, x];
                         currentArrayPos++;
                     }
                 }
             }
             // FVal = Filter.CalculateConvolutionValuesAndOffsets(w, out FOff); what does this function? Look prof solution
             int index = m2 * (w + 1);
             int indexStepRow = m2 * 2; //need to ignore border at the start of the row when i finished to iterate previously row
             for (int y = m2; y < InputImage.Height-m2; y++, index += indexStepRow) //starting from m2 and finishing at InputImage.Height-m2 to ignore borders.
             {
                 for (int x = m2; x < InputImage.Width-m2; x++)
                 {
                     int value = 0;
                     for(int k = 0; k < currentArrayPos; k++)
                     {
                         value = value + (InputImage[index + FOff[k]] * FVal[k]);
                     }
                     Result[index++] = value / Filter.Denominator;
                 }
             }
         }*/
    }

   
    [AlgorithmInfo("Smoothing (da immagine byte a immagine byte)", Category = "FEI")]
    public class SmoothingByteInt : ImageOperation<Image<byte>, Image<byte>>
    {
        [AlgorithmParameter]
        public int Size { get; set; }

        public override void Run()
        {
            ConvolutionFilter<int> filter = new ConvolutionFilter<int>(Size, Size * Size);
            for(int i = 0; i < Size * Size; i++)
            {
                filter[i] = 1;
            }
            Result = new ConvoluzioneByteInt(InputImage, filter).Execute().ToByteImage(PixelConversionMethod.Clip);
        }
    }

    [AlgorithmInfo("Sharpening", Category = "FEI")]
    public class SharpeningByteInt : ImageOperation<Image<byte>, Image<byte>>
    {
        private double weight = 0.0;
        [AlgorithmParameter]
        public double Weight {
            get
            {
                return weight;
            }
            set
            {
                if (value < 0.0 || value > 1.0)
                {
                    throw new Exception("The value cannot be greater than 1.0 and lower than 0.0.");
                }
                weight = value;
            }

        }

        [AlgorithmOutput]
        public Image<byte> Res { get; set; }

        public override void Run()
        {
            ConvolutionFilter<int> sharpeningFilter = new ConvolutionFilter<int>(3, 1);
            for (int i = 0; i < 3 * 3; i++)
            {
                if (i == 4)
                {
                    sharpeningFilter[i] = 8;
                } else
                {
                    sharpeningFilter[i] = -1;
                }
            }
            Image<byte> R = new ConvoluzioneByteInt(InputImage, sharpeningFilter).Execute().ToByteImage(PixelConversionMethod.Stretch);
            Res = R;
            Image<byte> WeightR = new LookupTableTransform<byte>(R, p => (p * Weight).RoundAndClipToByte()).Execute();
            Result = new ImageArithmetic(InputImage, WeightR, ImageArithmeticOperation.Add).Execute();
        }
    }
}
