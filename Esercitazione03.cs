﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;
using System.ComponentModel;

namespace PRLab.FEI
{

    [AlgorithmInfo("Stretch del contrasto es", Category = "FEI")]
    public class ContrastStretch2 : ImageOperation<Image<byte>, Image<byte>>
    {
        private int stretchDiscardPerc;

        [AlgorithmParameter]
        [DefaultValue(0)]
        public int StretchDiscardPercentage
        {
            get { return stretchDiscardPerc; }
            set
            {
                if (value < 0 || value > 100)
                    throw new ArgumentOutOfRangeException("Inserire un valore fra 0 and 100.");
                stretchDiscardPerc = value;
            }
        }

        public ContrastStretch2()
        {
        }

        public ContrastStretch2(Image<byte> inputImage)
          : base(inputImage)
        {
            StretchDiscardPercentage = 0;
        }

        public ContrastStretch2(Image<byte> inputImage, int stretchDiscard)
          : base(inputImage)
        {
            StretchDiscardPercentage = stretchDiscard;
        }

        public override void Run()
        {
            /*   Histogram hb = new HistogramBuilder(InputImage).Execute();

               int min = InputImage[0], max = InputImage[0];
               int i;
               int discardPixel = (InputImage.PixelCount * StretchDiscardPercentage) / 100;

               int sumSX = 0;
               for(i = 0; sumSX < discardPixel; i++)
               {
                   sumSX = sumSX + hb[i];
               }
               min = i;

               int sumDX = 0;
               for (i = 255; sumDX < discardPixel; i--)
               {
                   sumDX = sumDX + hb[i];
               }
               max = i;

               int diff = max - min;

               if (diff > 0)
               {
                   var op = new LookupTableTransform<byte>(InputImage,
                   p => (255 * (p - min) / diff).ClipToByte());
                   Result = op.Execute();
               }
               else Result = InputImage.Clone();*/

            Histogram hist = new HistogramBuilder(InputImage).Execute();
            // int pixDiscard = StretchDiscardPercentage * (InputImage.PixelCount / 100);
            int pixDiscard = InputImage.PixelCount * StretchDiscardPercentage / 100;
            int min = 0; int sumMin = 0;
            for (; sumMin < pixDiscard; min++)
            {
                sumMin = sumMin + hist[min];
            }

            int max = 255; int sumMax = 0;
            for (; sumMax < pixDiscard; max--)
            {
                sumMax = sumMax + hist[max];
            }
            int diff = max - min;
            if (diff > 0)
            {
                //Result = new LookupTableTransform<byte>(InputImage, p => (255 * ((p - min) / diff)).ClipToByte()).Execute();
                Result = new LookupTableTransform<byte>(InputImage, p => ((p - min) * 255 / diff).ClipToByte()).Execute();
            }
            else
            {
                Result = InputImage.Clone();
            }


        }
    }

    [AlgorithmInfo("Stretch del contrasto", Category = "FEI")]
    public class ContrastStretch : ImageOperation<Image<byte>, Image<byte>>
    {
        private int stretchDiscardPerc;

        [AlgorithmParameter]
        [DefaultValue(0)]
        public int StretchDiscardPercentage
        {
            get { return stretchDiscardPerc; }
            set
            {
                if (value < 0 || value > 100)
                    throw new ArgumentOutOfRangeException("Inserire un valore fra 0 and 100.");
                stretchDiscardPerc = value;
            }
        }

        public ContrastStretch()
        {
        }

        public ContrastStretch(Image<byte> inputImage)
          : base(inputImage)
        {
            StretchDiscardPercentage = 0;
        }

        public ContrastStretch(Image<byte> inputImage, int stretchDiscard)
          : base(inputImage)
        {
            StretchDiscardPercentage = stretchDiscard;
        }

        public override void Run()
        {
            Histogram hist = new HistogramBuilder(InputImage).Execute();
            int valuePerc = StretchDiscardPercentage * InputImage.PixelCount / 100;
            int i = 0;
            for (int sumsx = 0; sumsx < valuePerc && i < hist.Count(); i++)
            {
                sumsx = sumsx + hist[i];
            }

            int j = hist.Count() - 1;//256 - 1 is equals
            for (int sumdx = 0; sumdx < valuePerc && j > 0; j--)
            {
                sumdx = sumdx + hist[j];
            }
            int min = i, max = j;
            int diff = max - min;
            Result = new LookupTableTransform<byte>(InputImage, p => (255 * (p - min) / diff).ClipToByte()).Execute();
        }
    }


    [AlgorithmInfo("Equalizzazione istogramma", Category = "FEI")]
    public class HistogramEqualization : ImageOperation<Image<byte>, Image<byte>>
    {

        [AlgorithmOutput]
        public Histogram hout { get; set; }

        public HistogramEqualization()
        {
        }

        public HistogramEqualization(Image<byte> inputImage)
          : base(inputImage)
        {
        }

        public override void Run()
        {
            // Calcola l'istogramma
            Histogram hist = new HistogramBuilder(InputImage).Execute();
            // Ricalcola ogni elemento dell'istogramma come somma dei precedenti
            for (int i = 1; i < 256; i++)
                hist[i] += hist[i - 1];
            // Definisce la funzione di mapping e applica la LUT
             var op = new LookupTableTransform<byte>(InputImage,
                 p => (byte)(255 * hist[p] / InputImage.PixelCount));
             Result = op.Execute();
          /* same solution using for loop instead lut 
           * Result = new Image<byte>(InputImage.Width, InputImage.Height);
            for (int i = 0; i < InputImage.PixelCount; i++)
            {
                Result[i] = (255 * hist[InputImage[i]] / InputImage.PixelCount).ClipToByte();
            }*/
            hout = new HistogramBuilder(Result).Execute();
        }
    }

    [AlgorithmInfo("Operazione aritmetica", Category = "FEI")]
    public class ImageArithmetic : ImageOperation<Image<byte>, Image<byte>, Image<byte>>
    {
        [AlgorithmParameter]
        [DefaultValue(defaultOperation)]
        public ImageArithmeticOperation Operation { get; set; }
        const ImageArithmeticOperation defaultOperation = ImageArithmeticOperation.Difference;

        public ImageArithmetic()
        {
        }

        public ImageArithmetic(Image<byte> image1, Image<byte> image2, ImageArithmeticOperation operation)
          : base(image1, image2)
        {
            Operation = operation;
        }

        public ImageArithmetic(Image<byte> image1, Image<byte> image2)
          : this(image1, image2, defaultOperation)
        {
        }

        public override void Run()
        {
            Result = new Image<byte>(InputImage1.Width, InputImage1.Height);
            switch (Operation)
            {
                case ImageArithmeticOperation.Add:
                    for (int i = 0; i < InputImage1.PixelCount; i++)
                    {
                        Result[i] = (InputImage1[i] + InputImage2[i]).ClipToByte();
                    }
                    break;
                case ImageArithmeticOperation.And:
                    for (int i = 0; i < InputImage1.PixelCount; i++)
                    {
                        Result[i] = (InputImage1[i] & InputImage2[i]).ClipToByte();
                    }
                    break;
                case ImageArithmeticOperation.Average:
                    for (int i = 0; i < InputImage1.PixelCount; i++)
                    {
                        Result[i] = ((InputImage1[i] + InputImage2[i]) / 2).ClipToByte();
                    }
                    break;
                case ImageArithmeticOperation.Darkest:
                    for (int i = 0; i < InputImage1.PixelCount; i++)
                    {
                        Result[i] = InputImage1[i] < InputImage2[i] ? InputImage1[i] : InputImage2[i];
                    }
                    break;
                case ImageArithmeticOperation.Difference:
                    for (int i = 0; i < InputImage1.PixelCount; i++)
                    {
                        Result[i] = Math.Abs(InputImage1[i] - InputImage2[i]).ClipToByte();
                    }
                    break;
                case ImageArithmeticOperation.Lightest:
                    for (int i = 0; i < InputImage1.PixelCount; i++)
                    {
                        Result[i] = InputImage1[i] > InputImage2[i] ? InputImage1[i] : InputImage2[i];
                    }
                    break;
                case ImageArithmeticOperation.Or:
                    for (int i = 0; i < InputImage1.PixelCount; i++)
                    {
                        Result[i] = (InputImage1[i] | InputImage2[i]).ClipToByte();
                    }
                    break;
                case ImageArithmeticOperation.Subtract:
                    for (int i = 0; i < InputImage1.PixelCount; i++)
                    {
                        Result[i] = (InputImage1[i] - InputImage2[i]).ClipToByte();
                    }
                    break;
                case ImageArithmeticOperation.Xor:
                    for (int i = 0; i < InputImage1.PixelCount; i++)
                    {
                        Result[i] = (InputImage1[i] ^ InputImage2[i]).ClipToByte();
                    }
                    break;
                default:
                    Result = InputImage1;
                    break;
            }
        }
    }

    [AlgorithmInfo("Operazione aritmetica Rgb", Category = "FEI")]
    public class RgbImageArithmetic : ImageOperation<RgbImage<byte>, RgbImage<byte>, RgbImage<byte>>
    {
        [AlgorithmParameter]
        [DefaultValue(defaultOperation)]
        public ImageArithmeticOperation Operation { get; set; }
        const ImageArithmeticOperation defaultOperation = ImageArithmeticOperation.Difference;

        public RgbImageArithmetic()
        {
        }

        public RgbImageArithmetic(RgbImage<byte> image1, RgbImage<byte> image2, ImageArithmeticOperation operation)
          : base(image1, image2)
        {
            Operation = operation;
        }

        public RgbImageArithmetic(RgbImage<byte> image1, RgbImage<byte> image2)
          : this(image1, image2, defaultOperation)
        {
        }

        public override void Run()
        {
            Image<byte> red = new ImageArithmetic(InputImage1.RedChannel, InputImage2.RedChannel).Execute();
            Image<byte> green = new ImageArithmetic(InputImage1.GreenChannel, InputImage2.GreenChannel).Execute();
            Image<byte> blue = new ImageArithmetic(InputImage1.BlueChannel, InputImage2.BlueChannel).Execute();
            Result = new RgbImage<byte>(red, green, blue);
        }
    }
}
