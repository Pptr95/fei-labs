﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;
using System.ComponentModel;
using System.Drawing.Drawing2D;

namespace PRLab.FEI
{

  public abstract class TrasformazioneAffine<TImage> : ImageOperation<TImage, TImage>
    where TImage : ImageBase
  {
    protected TrasformazioneAffine()
    {
    }

    protected TrasformazioneAffine(TImage inputImage, double translationX, double translationY, double rotationDegrees, double scaleFactorX, double scaleFactorY, int resultWidth, int resultHeight)
      : base(inputImage)
    {
      TranslationX = translationX;
      TranslationY = translationY;
      RotationDegrees = rotationDegrees;
      ScaleFactorX = scaleFactorX;
      ScaleFactorY = scaleFactorY;
      ResultWidth = resultWidth;
      ResultHeight = resultHeight;
    }

    protected TrasformazioneAffine(TImage inputImage, double translationX, double translationY, double rotationDegrees)
      : this(inputImage, translationX, translationY, rotationDegrees, 1, 1, inputImage.Width, inputImage.Height)
    {
    }

    protected TrasformazioneAffine(TImage inputImage, double scaleFactor)
      : this(inputImage, 0, 0, 0, scaleFactor, scaleFactor, 0, 0)
    {
    }

    private double translationX;
    private double rotationDegrees;
    private double translationY;
    private double cx = 0.5;
    private double cy = 0.5;
    private double scaleFactorX = 1.0;
    private double scaleFactorY = 1.0;

    [AlgorithmParameter]
    [DefaultValue(0)]
    public double TranslationX { get { return translationX; } set { translationX = value; } }

    [AlgorithmParameter]
    [DefaultValue(0)]
    public double TranslationY { get { return translationY; } set { translationY = value; } }

    [AlgorithmParameter]
    [DefaultValue(0)]
    public double RotationDegrees { get { return rotationDegrees; } set { rotationDegrees = value; } }

    [AlgorithmParameter]
    [DefaultValue(0.5)]
    public double RotationCenterX { get { return cx; } set { cx = value; } }

    [AlgorithmParameter]
    [DefaultValue(0.5)]
    public double RotationCenterY { get { return cy; } set { cy = value; } }

    [AlgorithmParameter]
    [DefaultValue(1)]
    public double ScaleFactorX { get { return scaleFactorX; } set { scaleFactorX = value; } }

    [AlgorithmParameter]
    [DefaultValue(1)]
    public double ScaleFactorY { get { return scaleFactorY; } set { scaleFactorY = value; } }

    [AlgorithmParameter]
    [DefaultValue(0)]
    public int ResultWidth { get; set; }

    [AlgorithmParameter]
    [DefaultValue(0)]
    public int ResultHeight { get; set; }

        protected KeyValuePair<double, double> CalculateMapping(int xNew, int yNew, Image<byte> InputImage)
        {
            double rad = RotationDegrees * (Math.PI / 180);
            double kX = xNew - TranslationX;
            double kY = yNew - TranslationY;
            double xOld = (((1 / ScaleFactorX) * (Math.Cos(-rad))) * kX) + (((1 / ScaleFactorX) * (Math.Sin(-rad))) * kY);
            double yOld = (((1 / ScaleFactorY) * (-Math.Sin(-rad))) * kX) + (((1 / ScaleFactorY) * (Math.Cos(-rad))) * kY);
            return new KeyValuePair<double, double>(xOld, yOld);
            // return ImgOrBack(xOld, yOld, InputImage);
        }


        protected byte CalculateInterpolation(double xOld, double yOld, Image<byte> InputImage)
        {
            int xL = (int)xOld;
            int yL = (int)yOld;

            double wa = (xL + 1 - xOld) * (yL + 1 - yOld);
            double wb = (xOld - xL) * (yL + 1 - yOld);
            double wc = (xL + 1 - xOld) * (yOld - yL);
            double wd = (xOld - xL) * (yOld - yL);
            double sum = wa + wb + wc + wd;
            //check if pixel interpolation comes out of image
            if ((xL + 1) > InputImage.Height && (yL + 1) > InputImage.Width)
            {
                return InputImage[xL, yL];
            }
            else if ((xL + 1) > InputImage.Height && (yL + 1) <= InputImage.Width)
            {
                return ((((InputImage[xL, yL] * wa) + (InputImage[xL, yL + 1] * wc)))).RoundAndClipToByte();
            }
            else if ((xL + 1) <= InputImage.Height && (yL + 1) > InputImage.Width)
            {
                return ((((InputImage[xL, yL] * wa) + (InputImage[xL + 1, yL] * wb))) / sum).RoundAndClipToByte();
            }
            else
            {
                return ((((InputImage[xL, yL] * wa) + (InputImage[xL + 1, yL] * wb) + (InputImage[xL, yL + 1] * wc) + (InputImage[xL + 1, yL + 1] * wd))) / sum).RoundAndClipToByte();
            }
        }

        protected bool AreInteger(double x, double y)
        {
            return ((Math.Ceiling(x) == Math.Floor(x)) && (Math.Ceiling(y) == Math.Floor(y)));
        }
    }

    [AlgorithmInfo("Trasformazione affine (grayscale)", Category = "FEI")]
    public class TrasformazioneAffineGrayscale : TrasformazioneAffine<Image<byte>>
    {
        [AlgorithmParameter]
        [DefaultValue(0)]
        public byte Background { get; set; }

        public TrasformazioneAffineGrayscale(Image<byte> inputImage, double translationX, double translationY, double rotationDegrees, double scaleFactorX, double scaleFactorY, byte background, int resultWidth, int resultHeight)
          : base(inputImage, translationX, translationY, rotationDegrees, scaleFactorX, scaleFactorY, resultWidth, resultHeight)
        {
            Background = background;
        }

        public TrasformazioneAffineGrayscale(Image<byte> inputImage, double translationX, double translationY, double rotationDegrees, byte background)
          : base(inputImage, translationX, translationY, rotationDegrees)
        {
            Background = background;
        }

        public TrasformazioneAffineGrayscale(Image<byte> inputImage, double scaleFactor, byte background)
          : base(inputImage, scaleFactor)
        {
            Background = background;
        }

        public TrasformazioneAffineGrayscale()
        {
        }
        //TODO: rotation considering the center of image
        public override void Run()
        {
            Result = new Image<byte>(InputImage.Width, InputImage.Height);
            for (int y = 0; y < InputImage.Height; y++)
            {
                for (int x = 0; x < InputImage.Width; x++)
                {
                    KeyValuePair<double, double> point = CalculateMapping(y, x, InputImage);
                    double xOld = point.Key;
                    double yOld = point.Value;

                    if (xOld >= 0 && xOld < InputImage.Height && yOld >= 0 && yOld < InputImage.Width)
                    {
                        if (AreInteger(xOld, yOld))
                        {
                            Result[y, x] = InputImage[(int)xOld, (int)yOld];
                        }
                        else
                        {
                            Result[y, x] = CalculateInterpolation(xOld, yOld, InputImage);
                        }
                    }
                    else
                    {
                        Result[y, x] = Background;
                    }
                }
            }
        }
    }

    [AlgorithmInfo("Trasformazione affine (rgb)", Category = "FEI")]
    public class TrasformazioneAffineRgb : TrasformazioneAffine<RgbImage<byte>>
    {
        // [AlgorithmParameter]
        [DefaultValue(0)]
        public RgbPixel<byte> Background { get; set; }

        [AlgorithmParameter]
        [DefaultValue(0)]
        public byte BackgroundRed { get; set; }

        [AlgorithmParameter]
        [DefaultValue(0)]
        public byte BackgroundGreen { get; set; }

        [AlgorithmParameter]
        [DefaultValue(0)]
        public byte BackgroundBlue { get; set; }


        public TrasformazioneAffineRgb(RgbImage<byte> inputImage, double translationX, double translationY, double rotationDegrees, double scaleFactorX, double scaleFactorY, RgbPixel<byte> background, int resultWidth, int resultHeight)
            : base(inputImage, translationX, translationY, rotationDegrees, scaleFactorX, scaleFactorY, resultWidth, resultHeight)
        {
            Background = new RgbPixel<byte>(BackgroundRed, BackgroundGreen, BackgroundBlue);
        }

        public TrasformazioneAffineRgb(RgbImage<byte> inputImage, double translationX, double translationY, double rotationDegrees, RgbPixel<byte> background)
            : base(inputImage, translationX, translationY, rotationDegrees)
        {
            Background = new RgbPixel<byte>(BackgroundRed, BackgroundGreen, BackgroundBlue);
        }

        public TrasformazioneAffineRgb(RgbImage<byte> inputImage, double scaleFactor, RgbPixel<byte> background)
            : base(inputImage, scaleFactor)
        {
            Background = new RgbPixel<byte>(BackgroundRed, BackgroundGreen, BackgroundBlue);
        }

        public TrasformazioneAffineRgb()
        {
        }

        public override void Run()
        {
            /*First solution 
            Image<byte> red = new TrasformazioneAffineGrayscale(InputImage.RedChannel, TranslationX, TranslationY, RotationDegrees, BackgroundRed).Execute();
            Image<byte> green = new TrasformazioneAffineGrayscale(InputImage.GreenChannel, TranslationX, TranslationY, RotationDegrees, BackgroundGreen).Execute();
            Image<byte> blue = new TrasformazioneAffineGrayscale(InputImage.BlueChannel, TranslationX, TranslationY, RotationDegrees, BackgroundBlue).Execute();
            Result = new RgbImage<byte>(red, green, blue); */
             
            /*Optimized solution*/   
            Image<byte> red = new Image<byte>(InputImage.Width, InputImage.Height);
            Image<byte> green = new Image<byte>(InputImage.Width, InputImage.Height);
            Image<byte> blue = new Image<byte>(InputImage.Width, InputImage.Height);
            for (int y = 0; y < InputImage.Height; y++)
            {
                for (int x = 0; x < InputImage.Width; x++)
                {
                    KeyValuePair<double, double> point = CalculateMapping(y, x, InputImage.RedChannel);
                    double xOld = point.Key;
                    double yOld = point.Value;

                    if (xOld >= 0 && xOld < InputImage.Height && yOld >= 0 && yOld < InputImage.Width)
                    {
                        if (AreInteger(xOld, yOld))
                        {
                            red[y, x] = InputImage[(int)xOld, (int)yOld].Red;
                            green[y, x] = InputImage[(int)xOld, (int)yOld].Green;
                            blue[y, x] = InputImage[(int)xOld, (int)yOld].Blue;

                        } else {
                            red[y, x] = CalculateInterpolation(xOld, yOld, InputImage.RedChannel);
                            green[y, x] = CalculateInterpolation(xOld, yOld, InputImage.GreenChannel);
                            blue[y, x] = CalculateInterpolation(xOld, yOld, InputImage.BlueChannel);
                        }
                    } else {
                        red[y, x] = BackgroundRed;
                        green[y, x] = BackgroundGreen;
                        blue[y, x] = BackgroundBlue;
                    }
                }
            }
            Result = new RgbImage<byte>(red, green, blue);
        }
    }
    
}
