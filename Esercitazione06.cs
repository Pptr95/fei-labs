﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;
using System.ComponentModel;
using BioLab.GUI.UserControls;

namespace PRLab.FEI
{
    [AlgorithmInfo("Calcolo gradiente Prewitt", Category = "FEI")]
    public class CalcolaModuloGradiente : ImageOperation<Image<byte>, Image<int>>
    {
        [AlgorithmOutput]
        public Image<int> Theta { get; set; }

        ConvolutionFilter<int> fX = new ConvolutionFilter<int>(new int[] { 1, 0, -1,
                                                          1,  0, -1,
                                                          1, 0, -1 }, 3);
        ConvolutionFilter<int> fY = new ConvolutionFilter<int>(new int[] { 1, 1, 1,
                                                          0,  0, 0,
                                                          -1, -1, -1 }, 3);
        public override void Run()
        {
               Result = new Image<int>(InputImage.Width, InputImage.Height);
               Theta = new Image<int>(InputImage.Width, InputImage.Height);
               Image<int> convX = new ConvoluzioneByteInt(InputImage, fX).Execute();
               OnIntermediateResult(new AlgorithmIntermediateResultEventArgs(convX, "Gradiente della componente X con i filtri di Prewitt."));
               Image<int> convY = new ConvoluzioneByteInt(InputImage, fY).Execute();
               OnIntermediateResult(new AlgorithmIntermediateResultEventArgs(convY, "Gradiente della componente Y con i filtri di Prewitt."));
               for (int y = 0; y < InputImage.Height; y++)
               {
                   for (int x = 0; x < InputImage.Width; x++)
                   {
                       Result[y, x] = (int)(Math.Sqrt(Math.Pow(convX[y, x], 2) + Math.Pow(convY[y, x], 2)));
                       Theta[y, x] = (int)(Math.Atan2(convX[y, x], convY[y, x]));
                   }
               }
          /*  Result = new Image<int>(InputImage.Width, InputImage.Height);
            Image<int> convX = new ConvoluzioneByteInt(InputImage, fX, fX.Size / 2).Execute();
            OnIntermediateResult(new AlgorithmIntermediateResultEventArgs(convX, "Gradiente della componente X con i filtri di Prewitt."));
            Image<int> convY = new ConvoluzioneByteInt(InputImage, fY, fY.Size).Execute();
            OnIntermediateResult(new AlgorithmIntermediateResultEventArgs(convY, "Gradiente della componente Y con i filtri di Prewitt."));
            Image<int> gradienteTot = new Image<int>(InputImage.Width, InputImage.Height);
            for (int i = 0; i < InputImage.PixelCount; i++)
            {
                Result[i] = Math.Sqrt((convX[i] * convX[i]) + (convY[i] * convY[i])).RoundAndClipToByte();
            }*/
        }
    }

    [AlgorithmInfo("Trasformata distanza", Category = "FEI")]
    public class TrasformataDistanza : TopologyOperation<Image<int>>
    {

        [AlgorithmParameter]
        public MetricType Metric { get; set; }

        public TrasformataDistanza()
        {
        }

        public TrasformataDistanza(Image<byte> inputImage, byte foreground, MetricType metric)
          : base(inputImage, foreground)
        {
            Metric = metric;
        }

        public override void Run()
        {
            Result = new Image<int>(InputImage.Width, InputImage.Height);
            Image<int> r = Result;
            ImageCursor cursor = new ImageCursor(r, 1);
            if (Metric == MetricType.CityBlock)
            {
                do
                {
                    if (InputImage[cursor] == Foreground)
                    {
                        r[cursor] = Math.Min(r[cursor.West], r[cursor.North]) + 1;
                    }
                } while (cursor.MoveNext());

                do
                {
                    if (InputImage[cursor] == Foreground)
                    {
                        r[cursor] = Math.Min(Math.Min(r[cursor.East] + 1, r[cursor.South] + 1), r[cursor]);
                    }
                } while (cursor.MovePrevious());
            }
            else
            {

                do
                {
                    if (InputImage[cursor] == Foreground)
                    {
                        r[cursor] = Math.Min(Math.Min(Math.Min(r[cursor.West], r[cursor.North]), r[cursor.Northwest]), r[cursor.Northeast]) + 1;
                    }
                } while (cursor.MoveNext());

                do
                {
                    if (InputImage[cursor] == Foreground)
                    {
                        r[cursor] = Math.Min(Math.Min(Math.Min(Math.Min(r[cursor.East] + 1, r[cursor.South] + 1), r[cursor.Southwest] + 1), r[cursor.Southeast] + 1), r[cursor]);
                    }
                } while (cursor.MovePrevious());
            }
        }

    }
}
