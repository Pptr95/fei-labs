﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;
using System.ComponentModel;
using BioLab.GUI.UserControls;

namespace PRLab.FEI
{
    [BioLab.GUI.Forms.CustomAlgorithmPreviewOutput(typeof(BioLab.GUI.Forms.ContourExtractionViewer))]
    [AlgorithmInfo("Estrazione contornoX", Category = "FEI")]
  public class EstrazioneContorni : TopologyOperation<List<CityBlockContour>>
  {
        public override void Run()
        {
            Result = new List<CityBlockContour>();
            Image<bool> li = new Image<bool>(InputImage.Height, InputImage.Width);
            Image<byte> setUpImage = new ImageBorderFilling<byte>(InputImage, 1, (Foreground == 255 ? 0 : 1).ClipToByte(), false).Execute();
            // prof solution, using bitwise operatore ~
            // byte background = (byte)~Foreground; // ragionevole
            //Image<byte> setUpImage = (Image<byte>)new ImageBorderFilling<byte>(InputImage, 1, background, false).Execute();
            ImageCursor pixelStart = new ImageCursor(setUpImage);
            do
            {
                if(setUpImage[pixelStart] == Foreground && IsAvailableBorder(pixelStart, ref setUpImage, li))
                {
                    DrawContour(pixelStart, setUpImage, ref li);
                }
            } while (pixelStart.MoveNext());
        }

        private void DrawContour(ImageCursor pixelStart, Image<byte> setUpImage, ref Image<bool> blist)
        {
            blist[pixelStart] = true;
            CityBlockContour c = new CityBlockContour(pixelStart.X, pixelStart.Y);
            ImageCursor cursor = new ImageCursor(pixelStart);
            CityBlockDirection direction = CityBlockDirection.West;
            do
            {
                for (int i = 0; i < 3; i++)
                {
                    direction = CityBlockMetric.GetNextDirection(direction);
                    if (setUpImage[cursor.GetAt(direction)] == Foreground)
                    {
                        break;
                    }
                }
                c.Add(direction);
                cursor.MoveTo(direction);
                blist[cursor] = true;
                direction = CityBlockMetric.GetOppositeDirection(direction);
            } while (cursor != pixelStart);
            Result.Add(c);
        }

        private bool IsAvailableBorder(ImageCursor pixel, ref Image<byte> setUpImage, Image<bool> blist)
        {
            if(blist[pixel] == true)
            {
                return false;
            }
            if (setUpImage[pixel.East] == Foreground && setUpImage[pixel.West] == Foreground && setUpImage[pixel.South] == Foreground && setUpImage[pixel.North] == Foreground)
            {
                return false;
            } else if ((setUpImage[pixel.East] == Foreground || setUpImage[pixel.West] == Foreground || setUpImage[pixel.South] == Foreground || setUpImage[pixel.North] == Foreground && (blist[pixel] == false))) 
            {
                return true;
            } else
            {
                return false;
            }
        }
    }
}
